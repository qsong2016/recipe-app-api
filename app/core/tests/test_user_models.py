from django.test import TestCase
from django.contrib.auth import get_user_model


class ModelTests(TestCase):
    def test_create_user_with_email_success(self):
        """Test creating new use with email successful"""
        email = "test@blah.com"
        password = "testPass123"
        user = get_user_model().objects.create_user(email=email, password=password)

        self.assertEqual(user.email, email)
        self.assertTrue(user.check_password(password))

    def test_user_email_normalized(self):
        """Test new user email is nomalized"""
        email = "aass1g.s@NoWhereTOB.orG"
        user = get_user_model().objects.create_user(email, "aaa123")

        self.assertEqual(user.email, email.lower())

    def test_new_user_invalid_email(self):
        """Test new user with invalid email raises error"""
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user("", "aaa1")

    def test_create_super_user(self):
        """Test create a new superuser"""
        user = get_user_model().objects.create_superuser("aa@bb.com", "pp123")

        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)
